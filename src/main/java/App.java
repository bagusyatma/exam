public class App {
    public static void main(String[] args){
        Calendar myCalendar = new Calendar();
        myCalendar.setYear(2001);
        myCalendar.setMonth(12);

//        sorry, this method cannot be used yet
//        myCalendar.setWeek();

//        if you want to display all the months of the year
//        myCalendar.showYears();

//        if you want to display a specific month
        myCalendar.showMonth();
    }
}
